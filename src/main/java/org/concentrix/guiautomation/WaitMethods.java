package org.concentrix.guiautomation;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import keyword.constants.KeywordConstants;
import keyword.customobjects.TestSteps;


public class WaitMethods {
	
	

	private EventFiringWebDriver driver;
	private Wait<EventFiringWebDriver> wait;
	String waitload;

	public WaitMethods(EventFiringWebDriver driver) {
		this.driver = driver;
	}
	
	public WaitMethods(EventFiringWebDriver driver, Wait<EventFiringWebDriver> wait, String waitload) {
		this.driver = driver;
		this.wait = wait;
		this.waitload=waitload;
	}

	public WaitMethods(EventFiringWebDriver driver, Wait<EventFiringWebDriver> wait) {
		this.driver = driver;
		this.wait = wait;
	}

	public void waitForPageToLoad() {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return executeJavaScript("return document.readyState").equals("complete");
			}
		};

		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(20))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class);
		boolean isDocReady = wait.until(expectation);
		int i=1;
		while(!isDocReady && i <5){
			isDocReady = wait.until(expectation);
		}
		
		 try {
				PrintStream originalStream = System.out;

	    		PrintStream dummyStream    = new PrintStream(new OutputStream(){
	    		    public void write(int b) {
	    		        // do not do any action or print here
	    		    }
	    		});
	    		//System.setOut(dummyStream);
			 if("".equals(waitload) || waitload==null || waitload.equalsIgnoreCase("Y"))
				 	waitJQueryAngular();
			 System.setOut(originalStream);
		 } catch(Exception e)
		 {
		 }
	}
	
	// ----------------Code for waiting for jQuery and JS and Angular
	// JS-----------------

	public void waitForJQueryLoad() {
		
		
		// Wait for jQuery to load
		WebDriverWait wait = new WebDriverWait(driver, 35);
		ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				System.out.println(
						"JQuery State: " + ((JavascriptExecutor) driver).executeScript("return jQuery.active"));
				return ((Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
			}
		};

		// Get JQuery is Ready
		JavascriptExecutor jsExec = (JavascriptExecutor) driver;
		boolean jqueryReady = (Boolean) jsExec.executeScript("return jQuery.active==0");
       int i =1;
		
						// Wait JQuery until it is Ready!
			System.out.println("JQuery is NOT Ready!");
			// Wait for jQuery to load
			try {  
				wait.until(jQueryLoad);
				jqueryReady = (Boolean) jsExec.executeScript("return jQuery.active==0");
				Thread.sleep (1000);
				
			} catch (Exception e) {
				System.out.println("JQuery Ready? " + jqueryReady);
			
		}
		System.out.println("JQuery Ready? " + jqueryReady);
	}

	// Wait for Angular Load
	public void waitForAngularLoad() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		JavascriptExecutor jsExec = (JavascriptExecutor) driver;

		final String angularReadyScript = "return angular.element(document).injector().get('$http').pendingRequests.length === 0";

		// Wait for ANGULAR to load
		ExpectedCondition<Boolean> angularLoad = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				System.out.println(
						"Angular State: " + ((JavascriptExecutor) driver).executeScript(angularReadyScript).toString());
				return Boolean.valueOf(((JavascriptExecutor) driver).executeScript(angularReadyScript).toString());
			}
		};
		int i=1;
		// Get Angular is Ready
		boolean angularReady = Boolean.valueOf(jsExec.executeScript(angularReadyScript).toString());
		while (!angularReady && i<5) {
			// Wait ANGULAR until it is Ready!
				System.out.println("ANGULAR is NOT Ready!");
				try {
					// Wait for Angular to load
					wait.until(angularLoad);
					angularReady = Boolean.valueOf(jsExec.executeScript(angularReadyScript).toString());
					Thread.sleep (1000);
					i++;
				} catch (Exception e) {
					System.out.println("Angular Ready? " + angularReady);
				}
		}
//		System.out.println("Angular Ready? " + angularReady);
	}

	// Wait Until JS Ready
	public void waitUntilJSReady() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		JavascriptExecutor jsExec = (JavascriptExecutor) driver;

		// Wait for Javascript to load
		ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				System.out.println(
						"JS Document State: " + ((JavascriptExecutor) driver).executeScript("return document.readyState"));
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
						.equals("complete");
			}
		};
		int i=1;
		// Get JS is Ready
		boolean jsReady = (Boolean) jsExec.executeScript("return document.readyState").toString().equals("complete");
		while (!jsReady && i<5) {
			// Wait Javascript until it is Ready!
			try {
				// Wait for Javascript to load
				wait.until(jsLoad);
				jsReady = (Boolean) jsExec.executeScript("return document.readyState").toString().equals("complete");
				Thread.sleep (1000);
				i++;
			} catch (Exception e) {
				System.out.println("JS Ready ? " + jsReady);
			}
		}
//		System.out.println("JS Ready ? " + jsReady);
	}

	// Wait Until JQuery and JS Ready
	public void waitUntilJQueryReady() {
		JavascriptExecutor jsExec = (JavascriptExecutor) driver;
		waitUntilJSReady();
		// First check that JQuery is defined on the page. If it is, then wait
		// AJAX
		Boolean jQueryDefined = (Boolean) jsExec.executeScript("return typeof jQuery != 'undefined'");
		if (jQueryDefined == true) {
			// Pre Wait for stability (Optional)
			System.out.println("jQuery is defined on this site!");
			sleep(20);

			// Wait JQuery Load
			waitForJQueryLoad();

			// Wait JS Load
			waitUntilJSReady();

			// Post Wait for stability (Optional)
			sleep(20);
		} else {
//			System.out.println("jQuery is not defined on this site!");
		}
		
	}

	// Wait Until Angular and JS Ready
	public void waitUntilAngularReady() {
		JavascriptExecutor jsExec = (JavascriptExecutor) driver;

		// First check that ANGULAR is defined on the page. If it is, then wait
		// ANGULAR
		Boolean angularUnDefined = (Boolean) jsExec.executeScript("return window.angular === undefined");
		if (!angularUnDefined) {
			Boolean angularInjectorUnDefined = (Boolean) jsExec
					.executeScript("return angular.element(document).injector() === undefined");
			if (!angularInjectorUnDefined) {
				// Pre Wait for stability (Optional)
				// sleep(20);

				// Wait Angular Load
				waitForAngularLoad();

				// Wait JS Load
				waitUntilJSReady();

				// Post Wait for stability (Optional)
				 sleep(20);
			} else {
//				System.out.println("Angular injector is not defined on this site!");
			}
		} else {
//			System.out.println("Angular is not defined on this site!");
		}
	}

	// Wait Until JQuery Angular and JS is ready
	public void waitJQueryAngular() {
		waitUntilJQueryReady();
		waitUntilAngularReady();
	}

	public static void sleep(Integer seconds) {
		long secondsLong = (long) seconds;
		try {
			Thread.sleep(secondsLong);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
//-------------------------Till above code added for sync issue by chandu--------------------------------
	

	/*
	 * public WebElement getElement(String xpath) { WebElement element; try {
	 * element = getFluentWait(driver, KeywordConstants.globalTimeout)
	 * .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	 * if (element != null && element.isDisplayed()) { return element; } } catch
	 * (Exception e) { throw (e); }
	 * 
	 * return element; }
	 */

	public void waitForPageLoad() {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				System.out.println("----------Page loading = "
						+ executeJavaScript("return document.readyState").equals("complete"));
				return executeJavaScript("return document.readyState").equals("complete");

			}
		};

		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(Duration.ofSeconds(KeywordConstants.globalTimeout)).pollingEvery(Duration.ofSeconds(1))
				.ignoring(NoSuchElementException.class);
		boolean a = wait.until(expectation);

		// System.out.println("----------Page Loaded = " + a2);
	}

	public void waitForPageLoad(int timeout) {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				System.out.println("----------Page loading = "
						+ executeJavaScript("return document.readyState").equals("complete"));
				return executeJavaScript("return document.readyState").equals("complete");

			}
		};
		Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timeout))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class);
		boolean a2 = wait2.until(expectation);
		// System.out.println("----------Page Loaded = " + a2);
	}

	private Object executeJavaScript(String script) {
		JavascriptExecutor jsExecute = (JavascriptExecutor) driver;
		return jsExecute.executeScript(script);
	}
}
