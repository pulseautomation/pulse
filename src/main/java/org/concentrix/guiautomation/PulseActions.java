package org.concentrix.guiautomation;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.StringJoiner;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.concentrix.guiautomation.uiactions.ActionsLib;
import org.concentrix.guiautomation.uiactions.GenericLib;
import org.concentrix.guiautomation.uiactions.WebApplicationActions;
import org.concentrix.guiautomation.utils.PropertiesLib;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;



import keyword.constants.KeywordConstants;
import keyword.customobjects.Project;
import keyword.customobjects.TestSteps;
import keyword.databaseConnections.BankDBConnection;

public class PulseActions extends WebApplicationActions
{

	Project project;
	String projectName;
	String user;
	String password;

	public PulseActions(Project project)
	{
		super(project);
		this.project=project;
		projectName=project.getProjectName();
		
		PropertiesLib propertyLib;
		try {
			propertyLib = new PropertiesLib("KeywordArtifacts/" +projectName +"/Repository/ApplicationMessages.properties");
			Properties messageProperties=propertyLib.getProperties();
			configProp=propertyLib.mergeProperties(configProp, messageProperties);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	@Override
	public boolean executeNewKeywords(TestSteps testStep, String testName)
	 {
		boolean actualResult = false;
		switch (testStep.getAction().trim()) {
 
		
		case "reenteruniquetextwithoutclearing":
			this.actualResult = reEnterUniqueTextWithOutClearing(testStep);
			break;	
		case "writablemultiselectlistbox":
			this.actualResult = writableMultiSelectListBox(testStep);
			break;	
		case "selecttime":
			this.actualResult = selectTime(testStep);
			break;
		case "entercopiedpassword":
			this.actualResult = enterCopiedPassword(testStep);
			break;
		case "settargetdate":
			this.actualResult = setTargetDate(testStep);
			break;
		case "loginsystemadmin":
			this.actualResult = loginSystemAdmin(testStep);
			break;
		case "logincompanyadmin":
			this.actualResult = loginCompanyAdmin(testStep);
			break;
		case "loginqualityadmin":
			this.actualResult = loginQualityAdmin(testStep);
			break;
		case "loginaccountadmin":
			this.actualResult = loginAccountAdmin(testStep);
			break;
		case "logininstanceadmin":
			this.actualResult = loginInstanceAdmin(testStep);
			break;
		case "logingeographyadmin":
			this.actualResult = loginGeographyAdmin(testStep);
			break;
		case "loginagent":
			this.actualResult = loginAgent(testStep);
			break;
		case "enterrandomtextdata":
			this.actualResult = enterRandomTextData(testStep);
			break;
		case "selectdate":
			actualResult=selectDate(testStep);
			break;
		case "selectdatefromcalendar":
			this.actualResult = selectDateFromCalendar(testStep);
		case "verifylistvaluesnotpresent":
			this.actualResult = verifyListValuesNotPresent(testStep);
			break;
		case "verifylistvaluespresent":
			this.actualResult = verifyListValuesPresent(testStep);
			break;
		case "verifymessage":
			actualResult=verifyMessage(testStep);
			break;
		case "login":
			actualResult=login(testStep);
			break;
		case "clickelementbyscrolling":
			actualResult=clickElementByScrolling(testStep);
			break;
		case "selectmonth":
			actualResult=selectMonth(testStep);
			break;
		case "cleartextvalue":
			actualResult=clearTextValue(testStep);
			break;
		case "search":
			actualResult=search(testStep);
			break;
			
		case "joinstringwithspaces":
			actualResult=joinStringWithSpaces(testStep);
			break;
			
		default:
		{
			System.out.println("Keyword Does not Exists-" + testStep.getAction());
			actualResult= false;
			testStep.setExecutionDetails("No such keyword in framework. "
					+ "If keyword is defined please make sure that all the chars of keyword ar in small case in the Select Case statement ");
		}
		}
		return this.actualResult;
	 }
	
	/*
	 * The method used to login with an System Admin
	 * 
	 */
	public boolean loginSystemAdmin(TestSteps testStep) {
		boolean actualResult = false;
		try {
			user = configProp.getProperty("system.admin.usn");
			password = configProp.getProperty("system.admin.pwd");
			getElement(prop.getProperty("logininfo.username.textbox")).sendKeys(user);
			getElement(prop.getProperty("logininfo.next.button")).click();
			getElement(prop.getProperty("logininfo.password.textbox")).sendKeys(password);
			getElement(prop.getProperty("logininfo.signin.button")).click();
			actualResult = true;
			testCaseLog.accept(testStep, testStep.getTestStepDescription() + " user=" + user);
		} catch (Exception e) {
			this.exception = e.getClass().getSimpleName();
			testCaseLog.accept(testStep, e.toString());
		}

		return actualResult;

	}



	public boolean loginCompanyAdmin(TestSteps testStep) {
		boolean actualResult = false;
		try {
			user = configProp.getProperty("company.admin.usn");
			password = configProp.getProperty("company.admin.pwd");
			getElement(prop.getProperty("logininfo.username.textbox")).clear();
			getElement(prop.getProperty("logininfo.username.textbox")).sendKeys(user);
			getElement(prop.getProperty("logininfo.next.button")).click();
			getElement(prop.getProperty("logininfo.password.textbox")).sendKeys(password);
			getElement(prop.getProperty("logininfo.signin.button")).click();
			String currentUrl = driver.getCurrentUrl();
			actualResult = true;
			testCaseLog.accept(testStep, testStep.getTestStepDescription() + " user=" + user);
		} catch (Exception e) {
			this.exception = e.getClass().getSimpleName();
			testCaseLog.accept(testStep, e.toString());
		}

		return actualResult;
	}

	public boolean loginAccountAdmin(TestSteps testStep) {
		boolean actualResult = false;

		try {
			user = configProp.getProperty("account.admin.usn");
			password = configProp.getProperty("account.admin.pwd");
			getElement(prop.getProperty("logininfo.username.textbox")).sendKeys(user);
			getElement(prop.getProperty("logininfo.next.button")).click();
			getElement(prop.getProperty("logininfo.password.textbox")).sendKeys(password);
			getElement(prop.getProperty("logininfo.signin.button")).click();
			actualResult = true;
			testCaseLog.accept(testStep, testStep.getTestStepDescription() + " user=" + user);
		} catch (Exception e) {
			this.exception = e.getClass().getSimpleName();
			testCaseLog.accept(testStep, e.toString());
		}
		

		return actualResult;
	}

	public boolean loginInstanceAdmin(TestSteps testStep) {
		boolean actualResult = false;
		try {
			user = configProp.getProperty("instance.admin.usn");
			password = configProp.getProperty("instance.admin.pwd");
			getElement(prop.getProperty("logininfo.username.textbox")).sendKeys(user);
			getElement(prop.getProperty("logininfo.next.button")).click();
			getElement(prop.getProperty("logininfo.password.textbox")).sendKeys(password);
			getElement(prop.getProperty("logininfo.signin.button")).click();
			actualResult = true;
			testCaseLog.accept(testStep, testStep.getTestStepDescription());
		} catch (Exception e) {
			this.exception = e.getClass().getSimpleName();
			testCaseLog.accept(testStep, e.toString());
		}
		

		return actualResult;
	}

	public boolean loginGeographyAdmin(TestSteps testStep) {
		boolean actualResult = false;
		try {
			user = configProp.getProperty("geograpgy.admin.usn");
			password = configProp.getProperty("geograpgy.admin.pwd");
			getElement(prop.getProperty("logininfo.username.textbox")).sendKeys(user);
			getElement(prop.getProperty("logininfo.next.button")).click();
			getElement(prop.getProperty("logininfo.password.textbox")).sendKeys(password);
			getElement(prop.getProperty("logininfo.signin.button")).click();
			String currentUrl = driver.getCurrentUrl();
			// Assert.assertEquals(currentUrl, url+"Admin/Default.aspx");
			actualResult = true;
			testCaseLog.accept(testStep, testStep.getTestStepDescription());
		} catch (Exception e) {
			this.exception = e.getClass().getSimpleName();
			testCaseLog.accept(testStep, e.toString());
		}
		return actualResult;
	}
	
	
	
	public boolean click(TestSteps testStep) {
		boolean actualResult = false;
		WebElement element = null;
		String xpath = testStep.getKey();
		try {
			if(xpath.contains("{0}"))
			{
				xpath = MessageFormat.format(xpath, testStep.getTestData());
			}
			element = getElementPresent(xpath);
			element.click();
			actualResult = true;
			testCaseLog.accept(testStep,testStep.getTestStepDescription());
		} 
		catch (Exception e) {
			this.exception = e.toString();
			testCaseLog.accept(testStep,e.getMessage());
			try {
				clickElementByScrolling(element);
				if (element == null) {
					return false;
				}
				actualResult = true;
			} catch (InterruptedException e1) {
								e1.printStackTrace();
								
			}
			
		}
		return actualResult;
	}
	
//	public boolean logOut(TestSteps testStep) {
//		boolean actualResult = false;
//		try {
//			switchToParentFrame(testStep);
//			WebElement element = getElement(prop.getProperty("logoutinfo.logout.button"));
//			element.click();
//			System.out.println("Logged Out");
////			ActionsLib actionsLib = new ActionsLib(driver);
//			waitForPageToLoad();
//			actualResult = true;
//		} catch (Exception e) {
//			// e.printStackTrace();
//		}
//		return actualResult;
//	}

	/**
	 * This keyword will logout the user from application and close all the
	 * browsers opened by automation
	 */
	public boolean closeBrowser(TestSteps testStep) {

		boolean actualResult = false;
		try {
			logOut(testStep);
		} catch (Exception e) {
			log.error("Unable to logout" + testStep.getKey() + " " + this.exception);
		} finally {
			try {
				if(driver !=null)
				driver.quit();
				actualResult = true;
				testCaseLog.accept(testStep, testStep.getTestStepDescription());
				log.info("Successfully closed Browser window: " + testStep.getKey());
			} catch (Exception e) {
				// e.printStackTrace();
			}
		}
		return actualResult;
	}

	/**
	 * Enters the given data in the 'test_data' column in the test script.
	 * Before entering the given data it will clear the text if present from the
	 * text box in the application
	 * 
	 * @param testStep
	 * @return
	 */
public boolean writableMultiSelectListBox(TestSteps testStep) {
		boolean actualResult = false;
		WebElement element = null;
		try {
			String xpath = testStep.getKey();
				element = getElement(xpath);
				String value = getVariableValue(this.runTimeMap,testStep.getTestData());
				
				if (element != null) {
					Actions actions = new Actions(driver);
					actions.moveToElement(element);
					actions.click();
					Thread.sleep(1000);
					actions.sendKeys(value);
					actions.build().perform();
					Thread.sleep(1000);
					actualResult = true;
					testCaseLog.accept(testStep, "Successfully entered the text: "+ testStep.getTestStepDescription());
				} else {
					throw new Exception("Element not present");
				}

			
		} catch (Exception e) {
			e.printStackTrace();
			this.exception = e.toString();
			testCaseLog.accept(testStep, e.toString());
		}
		return actualResult;

	}
	
	@Override
	public boolean pressEnter(TestSteps testStep) {
		WebElement element = null;
		boolean actualResult = false;
		try {
				if (isDisplayed(testStep.getKey())) {
					element = getElement(testStep.getKey());
					Actions actions = new Actions(driver);
					actions.moveToElement(element);
					actions.sendKeys(Keys.ENTER);
					actions.build().perform();
					actualResult = true;
					testCaseLog.accept(testStep, "Successfully Switched to Frame: " +testStep.getTestStepDescription());
				}
			

		} catch (Exception e) {
			this.exception = e.toString();
			testCaseLog.accept(testStep, "Unable to switch to Frame  " + e.toString());
		}
		return actualResult;
	}
	
	
	/**
	 * This is method is written specifically for PULSE application
	 * This method is created to get the Monday as the start date for target creation 
	 * and next Sunday of start date as the end date of target as we can not create a new target if active 
	 * target is available in given date range.
	 * @param testStep
	 * @return
	 */
	public boolean setTargetDate(TestSteps testStep){
		boolean actualResult = false;
		try{
			DateTimeFormatter format =  DateTimeFormatter.ofPattern("dd,MMM,yyyy"); 
			LocalDate date = LocalDate.now();
			DayOfWeek dayOfWeek = date.getDayOfWeek();
			if(testStep.getTestData().trim().equalsIgnoreCase("targetStartDate")){
				int daysToNextMonday = 7-dayOfWeek.getValue()+1;
				LocalDate startDate = date.plusDays(daysToNextMonday);
				this.runTimeMap.put("targetstartdate", startDate.format(format));
				testCaseLog.accept(testStep, testStep.getTestStepDescription() + " :" + startDate.format(format));
			}else if(testStep.getTestData().trim().equalsIgnoreCase("targetEndDate")){
				int daysToSundayAfterNextMonday = 7-dayOfWeek.getValue()+7;
				LocalDate endDate = date.plusDays(daysToSundayAfterNextMonday);
				this.runTimeMap.put("targetenddate", endDate.format(format));
				testCaseLog.accept(testStep, testStep.getTestStepDescription() + " :" + endDate.format(format));
			}
			actualResult = true;
		}catch(Exception e){
			this.exception = e.getClass().getName() + e.getMessage();
			testCaseLog.accept(testStep, e.toString());
		}
		return actualResult;
	}

	public boolean selectTime(TestSteps testStep){
		WebElement element = null;
		boolean actualResult = false;
		String hour = null;
		String minute=null;
		
		int min=0;
		try {
			
			if(testStep.getTestInput1().toLowerCase().contains("systemtime"))
			{
				StringTokenizer st = new StringTokenizer(testStep.getTestData(), "+|-", true);
				String refDate = st.nextToken();
				LocalDateTime localTime = LocalDateTime.now();
				
				String arithmeticOperator = null;
				String minutesTobeAdded = null;
				
				try {
					arithmeticOperator = st.nextToken();
					
					minutesTobeAdded = st.nextToken();
					if(arithmeticOperator !=null)
					{
						localTime=localTime.plusMinutes(Integer.parseInt(minutesTobeAdded));
					}
					hour=String.valueOf(localTime.getHour());
					minute=String.valueOf(localTime.getMinute());
//					int minToBeSelected=Integer.parseInt(minute);
					Double minToBeSelected=null;
					int minuteinInteger=Integer.parseInt(minute);
					if(minuteinInteger<55)
					{
						minToBeSelected=Math.floor((minuteinInteger / 5) * 5) + 5;
					}
					else
					{
						minToBeSelected=0.0;
					}
					 min = minToBeSelected.intValue();
					 if(min==0)
					 {
						 minute="00";
					 }
					 else
					 {
						 minute=String.valueOf(min);
					 }
					 
				} catch (Exception e) {
				}
			}
			else
			{
			String[] testDataParams = testStep.getTestData().split(":");
			 hour = testDataParams[0];
			 minute = testDataParams[1];
			}
			List<WebElement> clockList = null;
			clockList =  getElements("//div[@class='clockpicker-dial clockpicker-hours']//div[text()='"+ hour +"']");
//			clockList = driver.findElements(By.xpath("//div[@class='clockpicker-dial clockpicker-hours']//div[text()='"+ hour +"']"));
			for (WebElement clock : clockList) {
				for (int i = 0; i < 3; i++) {
					try {
						if (clock.isDisplayed()) {
							System.out.println(clock.getText());
							if(clock.getText().equals(hour))
							{
								clock.click();
								break;
							}
						}
					} catch (StaleElementReferenceException e) {
						Thread.sleep(500);
						System.out.println("Clock : StaleElementReferenceException");
					}
				}
			}
//			Thread.sleep(2000);
			Thread.sleep(800);
			clockList = getElements("//div[@class='clockpicker-dial clockpicker-minutes']//div[text()='"+ minute +"']");
//			clockList = driver.findElements(By.xpath("//div[@class='clockpicker-dial clockpicker-minutes']//div[text()='"+ minute +"']"));
			for (WebElement clock : clockList) {
				for (int i = 0; i < 3; i++) {
					try {
						if (clock.isDisplayed()) {
							System.out.println(clock.getText());
							if(clock.getText().equals(minute))
							{
								clock.click();
								actualResult = true;
								break;
							}
							
							testCaseLog.accept(testStep, testStep.getTestStepDescription());
							break;
						}
					} catch (StaleElementReferenceException e) {
						Thread.sleep(500);
						System.out.println("Clock : StaleElementReferenceException");
					}
				}
			}
			
		} catch (Exception e) {
			this.exception = e.toString();
			testCaseLog.accept(testStep, e.toString());
		}
			
		return actualResult;
	}
	
	/**
	 * This keyword should be used to reenter the saved data from the previous
	 * steps.
	 * 
	 * @param testStep
	 * @return
	 */
	public boolean reEnterUniqueTextWithOutClearing(TestSteps testStep) {
		boolean actualResult = false;
		WebElement element = null;
		try {
				element = getElement(testStep.getKey());
				String savedData = getVariableValue(this.runTimeMap,testStep.getTestData());
				element.sendKeys(savedData);
				testCaseLog.accept(testStep,savedData + " - Text Entered");
				actualResult = true;
			
		} catch (Exception e) {
			this.exception = e.toString();
			testCaseLog.accept(testStep, e.toString());
		}
		return actualResult;

	}

	/**
	 * This keyword should be used to reenter the password during the creation
	 * of user
	 * 
	 * @param testStep
	 * @return
	 */
	public boolean enterCopiedPassword(TestSteps testStep) {
		boolean actualResult = false;
		WebElement element = null;
		try {
//			String xpath = prop.getProperty(testStep.getKey());
//			if (xpath != null && xpath != "" && !xpath.isEmpty() && element == null) {
				element = getElement(testStep.getKey());
				String savedData = getVariableValue(this.runTimeMap,testStep.getTestData());
				// element.clear();
				if (savedData == null) {
					savedData = testStep.getTestData();
				}
				String[] passwordArray = {};
				String password = "";
				if (savedData.contains(":")) {
					passwordArray = savedData.split(":");
					for (int i = 1; i < passwordArray.length; i++) {
						password = password.concat(passwordArray[i]);
						password = password.concat(":");
					}
					password = password.substring(0, password.length() - 1);
				}
				element.clear();
				element.sendKeys(password);
				testCaseLog.accept(testStep, password + "-Text Entered");
				actualResult = true;
		} catch (Exception e) {
			this.exception = e.toString();
			testCaseLog.accept(testStep, e.toString());
		}
		return actualResult;

	}

	/**
	 * Will find the given text in the table by looping row by row. When the
	 * value matches, the cell will be clicked. Used for clicking the text which
	 * is link in the table
	 * 
	 * @param testStep
	 * @return
	 */
	@Override
	public boolean searchUniqueTextInTableAndClick(TestSteps testStep) {
		boolean actualResult = false;
		WebElement element = null;
		try {
			String searchValue = getVariableValue(this.runTimeMap,testStep.getTestData());
			if (searchValue == null)
				searchValue = testStep.getTestData();
			String xpathForTable = testStep.getKey();

				// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathForTable)));
				WebElement tablename = getElement(xpathForTable);
				List<WebElement> tablerows = tablename.findElements(By.tagName("tr"));
				int rowscount = tablerows.size();
				loopAllRows: for (int row = 0; row < rowscount; row++) {
					List<WebElement> tablecolumns = tablerows.get(row).findElements(By.tagName("td"));
					int columnscount = tablecolumns.size();
					System.out.println("Number of cells In Row " + row + " are " + columnscount);
					for (int column = 0; column < columnscount; column++) {
						String celltext = tablecolumns.get(column).getText();
						if (celltext.contains(searchValue)) {
							try {
								tablecolumns.get(column)
										.findElement(By.xpath("//a[contains(text(), '" + searchValue + "')]")).click();
							} catch (Exception e) {
								System.out.println("Link is not defined in the table cell. So clicking the td directly in next step");
								tablecolumns.get(column).click();
							}
							
							actualResult = true;
							testCaseLog.accept(testStep, testStep.getTestStepDescription());
							break loopAllRows;
						}
					}
				}
				if (!actualResult)
					throw new Exception("Element not found");
			
		} catch (Exception e) {
			this.exception = e.toString();
			testCaseLog.accept(testStep, e.toString());
		}

		return actualResult;
	}
	
	
	
	/*
	 * Method used to verify text on the grid element
	 * 
	 * @see org.concentrix.guiautomation.uiactions.WebApplicationActions#
	 * clickOnGridElement(keyword.customobjects.TestSteps)
	 */
	@Override
	public boolean verifyTextOnGrid(TestSteps testStep) {
		boolean actualResult = false;
		WebElement element = null;
		String text = null;
		ActionsLib actionsLib = new ActionsLib(driver);
		int paging = 0;
		int total = 0;
		try {

			String xpath = testStep.getKey();
				String[] tableValues = testStep.getTestData().split(";");
				if (isDisplayed(xpath + "//tbody/tr")) {
					total = getElements(xpath + "//tbody/tr").size();
					for (int i = 1; i <= total; i++) {
					
						if (isDisplayed(xpath + "//tbody//tr[" + i + "]//td[" + tableValues[0] + "]//a")
								&& isDisplayed(xpath + "//tbody//tr[" + i + "]//td[" + tableValues[0] + "]")) {
							element = driver.findElement(
									By.xpath(xpath + "//tbody//tr[" + i + "]//td[" + tableValues[0] + "]"));
						} else if (isDisplayed(xpath + "//tbody//tr[" + i + "]//td[" + tableValues[0] + "]")) {
							element = driver.findElement(
									By.xpath(xpath + "//tbody//tr[" + i + "]//td[" + tableValues[0] + "]"));
						}
						text = element.getText().trim();
						if (text.contains(tableValues[1].trim())) {
							actualResult = true;
							testCaseLog.accept(testStep, testStep.getTestStepDescription());
							break;
						}
					}
				} else if (isDisplayed(xpath + "//ul//li")) {
					if (isDisplayed(xpath + "//ul//li//div//small[contains(text(),'" + tableValues[0].trim() + "')]")) {
						element = driver.findElement(
								By.xpath(xpath + "//li//div//small[contains(text(),'" + tableValues[0] + "')]"));
					}
					text = element.getText().trim();
					if (text.contains(tableValues[0].trim())) {
						actualResult = true;
						testCaseLog.accept(testStep, testStep.getTestStepDescription());
					}
				}

		} catch (Exception e) {
			this.exception = e.toString();
			testCaseLog.accept(testStep, e.toString());
		}

		return actualResult;
	}

	/**
	 * The below method will try to find web element on web table and click by
	 * index (any type) eg: will click on element on web table page
	 * 
	 * @param testStep
	 * @return
	 */
@Override	
public boolean clickOnDataInTableByIndex(TestSteps testStep) {
		boolean actualResult = false;
		WebElement element = null;
		try {
			String xpath = testStep.getKey();
				getElement(xpath);
				String[] tableValues = testStep.getTestData().split(";");

				try {
					element = driver.findElement(
							By.xpath(xpath + "//tbody//tr[" + tableValues[0] + "]//td[" + tableValues[1] + "]//a"));
				} catch (Exception e) {
					//System.out.println("No such element");
				}
				if (element == null){
				if (isDisplayed(xpath + "//tbody//tr[" + tableValues[0] + "]//td[" + tableValues[1] + "]/button")) {
					element = driver.findElement(
							By.xpath(xpath + "//tbody//tr[" + tableValues[0] + "]//td[" + tableValues[1] + "]/button"));
				} else if (isDisplayed(xpath + "//tbody//tr[" + tableValues[0] + "]//td[" + tableValues[1] + "]")) {
					element = driver.findElement(
							By.xpath(xpath + "//tbody//tr[" + tableValues[0] + "]//td[" + tableValues[1] + "]"));
				}
				}
				element.click();
				actualResult = true;
				testCaseLog.accept(testStep, testStep.getTestStepDescription());
		} catch (Exception e) {
			this.exception = e.toString();
			testCaseLog.accept(testStep, e.toString());
		}

		return actualResult;
	}

	public boolean loginQualityAdmin(TestSteps testStep) {
		boolean actualResult = false;
		try {
			user = configProp.getProperty("quality.admin.usn");
			password = configProp.getProperty("quality.admin.pwd");
			getElement(prop.getProperty("logininfo.username.textbox")).clear();
			getElement(prop.getProperty("logininfo.username.textbox")).sendKeys(user);
			getElement(prop.getProperty("logininfo.next.button")).click();
			getElement(prop.getProperty("logininfo.password.textbox")).sendKeys(password);
			getElement(prop.getProperty("logininfo.signin.button")).click();
			actualResult = true;
			testCaseLog.accept(testStep, testStep.getTestStepDescription());
		} catch (Exception e) {
			this.exception = e.getClass().getSimpleName();
			testCaseLog.accept(testStep, e.toString());
		}
		return actualResult;
	}
	
	public boolean loginAgent(TestSteps testStep) {
		boolean actualResult = false;
		try {
			user = configProp.getProperty("agent.usn");
			password = configProp.getProperty("agent.pwd");
			getElement(prop.getProperty("logininfo.username.textbox")).clear();
			getElement(prop.getProperty("logininfo.username.textbox")).sendKeys(user);
			getElement(prop.getProperty("logininfo.next.button")).click();
			getElement(prop.getProperty("logininfo.password.textbox")).sendKeys(password);
			getElement(prop.getProperty("logininfo.signin.button")).click();
			actualResult = true;
			testCaseLog.accept(testStep, testStep.getTestStepDescription());
		} catch (Exception e) {
			this.exception = e.getClass().getSimpleName();
			testCaseLog.accept(testStep, e.toString());
		}
		return actualResult;
	}
	
	public boolean loginTL(TestSteps testStep) {
		boolean actualResult = false;
		try {
			user = configProp.getProperty("tl.usn");
			password = configProp.getProperty("tl.pwd");
			getElement(prop.getProperty("logininfo.username.textbox")).clear();
			getElement(prop.getProperty("logininfo.username.textbox")).sendKeys(user);
			getElement(prop.getProperty("logininfo.next.button")).click();
			getElement(prop.getProperty("logininfo.password.textbox")).sendKeys(password);
			getElement(prop.getProperty("logininfo.signin.button")).click();
			String currentUrl = driver.getCurrentUrl();
			actualResult = true;
			testCaseLog.accept(testStep, testStep.getTestStepDescription());
		} catch (Exception e) {
			this.exception = e.getClass().getSimpleName();
			testCaseLog.accept(testStep, e.toString());
		}
		return actualResult;
	}
	
	public boolean loginAM(TestSteps testStep) {
		boolean actualResult = false;
		try {
			user = configProp.getProperty("account.am.usn");
			password = configProp.getProperty("account.am.pwd");
			getElement(prop.getProperty("logininfo.username.textbox")).clear();
			getElement(prop.getProperty("logininfo.username.textbox")).sendKeys(user);
			getElement(prop.getProperty("logininfo.next.button")).click();
			getElement(prop.getProperty("logininfo.password.textbox")).sendKeys(password);
			getElement(prop.getProperty("logininfo.signin.button")).click();
			actualResult = true;
			testCaseLog.accept(testStep, testStep.getTestStepDescription());
		} catch (Exception e) {
			this.exception = e.getClass().getSimpleName();
			testCaseLog.accept(testStep, e.toString());
		}

		return actualResult;
	}
	
	public boolean loginOM(TestSteps testStep) {
		boolean actualResult = false;
		try {
			user = configProp.getProperty("om.usn");
			password = configProp.getProperty("om.pwd");
			getElement(prop.getProperty("logininfo.username.textbox")).clear();
			getElement(prop.getProperty("logininfo.username.textbox")).sendKeys(user);
			getElement(prop.getProperty("logininfo.next.button")).click();
			getElement(prop.getProperty("logininfo.password.textbox")).sendKeys(password);
			getElement(prop.getProperty("logininfo.signin.button")).click();
			String currentUrl = driver.getCurrentUrl();
			actualResult = true;
			testCaseLog.accept(testStep, testStep.getTestStepDescription());
		} catch (Exception e) {
			this.exception = e.getClass().getSimpleName();
			testCaseLog.accept(testStep, e.toString());
		}
		return actualResult;
	}
	@Override
	public boolean select_menu(TestSteps testStep) 
  	{
		boolean actualResult = false;
		String menuLinkName=testStep.getTestData();
		driver.switchTo().defaultContent();
    	String xpath="//li/a[span[@class='menu-item-parent' and  text()='"+testStep.getTestInput1()+"']]";
    	try
		{
    		WebElement menuElement = null;
			try {
			boolean	menuLoaded = actionsLib.assertTextPresent(xpath, testStep.getTestInput1());
			if (menuLoaded) {
				menuElement = driver.findElement(By.xpath(xpath));
			}
				
			} catch (Exception e) {
			}
    		if(menuElement != null )
    		{
    			menuElement.click();
    			System.out.println("Clicked Parent Menu");
    			driver.switchTo().frame("pulse_frame");
    	    	actualResult=true;
    			testCaseLog.accept(testStep, testStep.getTestStepDescription());
    		}
			
		}
		catch (Exception e)
		{
				e.printStackTrace();
				testCaseLog.accept(testStep, e.toString());
		}
    
    	return actualResult;
   		
  	}
	
	@Override
    public boolean select_SubMenu(TestSteps testStep)
     {
        boolean actualResult = false;
        driver.switchTo().defaultContent();
        String[] testData=testStep.getTestInput1().split(";");
        String parentMenuLinkText=testData[0];
        String subMenuLinkText=testData[1];
        StringJoiner xpath= new StringJoiner("|");
        xpath.add("//li/a[span[@class='menu-item-parent' and  text()='"+parentMenuLinkText+"']]/following-sibling::ul[contains(@style,'display: block')]/li/a[text()='"+subMenuLinkText+"']");
        xpath.add("//li/a[span[@class='menu-item-parent' and  text()='"+parentMenuLinkText+"']]/following-sibling::ul[contains(@style,'display: block')]/li/a/span[text()='"+subMenuLinkText+"']");
  
       String submenu_xpath=xpath.toString();
       String parentmenu_xpath="//li/a[span[@class='menu-item-parent' and  text()='"+parentMenuLinkText+"']]";
  try
  {
      WebElement submenuElement = null;
      try {
         boolean elementExists = false;
                 elementExists = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(5))
                .pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class)
                .ignoring(WebDriverException.class).until(new ExpectedCondition<Boolean>() {
                 public Boolean apply(WebDriver driver) {
                      try {
                           WebElement element = driver.findElement(By.xpath(submenu_xpath));
                           String elementText = element.getText();
                         System.out.println("Actual Text " + elementText);
                        if (elementText.trim().toLowerCase().contains(subMenuLinkText.trim().toLowerCase())) {
                         return true;
                         } else {
                             return false;
                                 }
                         }

                      catch (Exception e) {
                        return false;
                         }
                      }
                     });
            if (elementExists) {
                 submenuElement = driver.findElement(By.xpath(submenu_xpath));
            }
        
      } catch (Exception e) {
     
      }
      if(submenuElement != null )
      {
          submenuElement.click();
          System.out.println("Clicked Sub Menu");
      }
      else
      {
          PulseLib lib= new PulseLib(driver);
          lib.click(parentmenu_xpath);
          Thread.sleep(1000);
          lib.click(submenu_xpath);
          System.out.println("Clicked Sub Menu");
      }
      driver.switchTo().frame("pulse_frame");
      Thread.sleep(1000);
      actualResult=true;
      testCaseLog.accept(testStep, testStep.getTestStepDescription());
  }
  catch (Exception e)
  {
          e.printStackTrace();
          testCaseLog.accept(testStep, e.toString());
  }

  return actualResult;
    
}

	public boolean select_NestedSubmenu(TestSteps testStep) 
  	{
		boolean actualResult = false;
		driver.switchTo().defaultContent(); 
		String[] testData=testStep.getTestInput1().split(";");
		String parentMenuLinkText=testData[0];
		String subMenuLinkText=testData[1];
//    	String submenu_xpath="//li/a[span[@class='menu-item-parent' and  text()='"+parentMenuLinkText+"']]/following-sibling::ul[contains(@style,'display: block')]/li/a[text()='"+subMenuLinkText+"']";
//    	String parentmenu_xpath="//li/a[span[@class='menu-item-parent' and  text()='"+parentMenuLinkText+"']]";
//    	try
//		{
//    		WebElement submenuElement = null;
//			try {
//				submenuElement = driver.findElement(By.xpath(submenu_xpath));
//			} catch (Exception e) {
//			
//			}
//    		if(submenuElement != null )
//    		{
//    			submenuElement.click();
//    			System.out.println("Clicked Sub Menu");
//    		}
//    		else
//    		{
//    			PulseLib lib= new PulseLib(driver);
//    			lib.click(parentmenu_xpath);
//    			lib.click(submenu_xpath);
//    			System.out.println("Clicked Sub Menu");
//    		}
//			driver.switchTo().frame("pulse_frame");
//			Thread.sleep(1000);
//	    	actualResult=true;
//			testCaseLog.accept(testStep, testStep.getTestStepDescription());
//		}
//		catch (Exception e)
//		{
//				e.printStackTrace();
//				testCaseLog.accept(testStep, e.toString());
//		}
    
    	return actualResult;
   		
  	}
	
	public boolean selectdropdownvalue(TestSteps testStep) {
		boolean actualResult = false;
		WebElement element = null;
		Actions action = new Actions(driver);
		try {
			final String xpath = testStep.getKey();
			Objects.requireNonNull(xpath, "XPATH Undefined");
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
					.withTimeout(Duration.ofSeconds(KeywordConstants.globalTimeout))
					.ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class)
					.ignoring(ElementNotVisibleException.class);

			boolean elementExists = wait.until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					boolean returnval = false;
					try {
						WebElement element = driver.findElement(By.xpath(xpath));
						// js.executeScript("arguments[0].scrollIntoView(true);",
						// element);
						if (element != null && element.isDisplayed() && element.isEnabled()) {
							Select select = new Select(element);
							if (select.getOptions().size() > 0)
								returnval = true;
						}
						return returnval;
					} catch (Exception e) {
						return returnval;
					}
				}
			});
			if (elementExists == true) {
				element = getElement(xpath);
				Select select = new Select(element);
				String[] dropDownValues = testStep.getTestData().split(";");
				System.out.println(dropDownValues.length);

					for (int i = 0; i < dropDownValues.length; i++) {
						if (element.isEnabled()) {
							String value = getVariableValue(runTimeMap, dropDownValues[i]).toLowerCase();
							
							List <WebElement> options  = select.getOptions();
							int m=0;
								for (WebElement we : options) { 							
									if(we.getText().trim().toLowerCase().contains(value))
									{
										select.selectByIndex(m);
										actualResult = true;
										testCaseLog.accept(testStep, testStep.getTestStepDescription());
										break;
									}
									m++;
								}
							if(actualResult==false)
								throw new Exception(value +"  Not found in dropdown ");
						} else {
							actualResult=false;
							throw new Exception("Dropdown is not enabled");
						}
					}
				
			} 
		} catch (Exception e) {
			this.exception = e.getClass().getName();
			testCaseLog.accept(testStep, e.toString());
		}
		return actualResult;
	}

	/**
	 * The below method will select multiple item by its visible text from the
	 * listbox
	 * 
	 * @param testStep
	 * @return
	 */
	public boolean selectByVisibleText(TestSteps testStep) {
		boolean actualResult = false;
		WebElement element = null;
		try {
			final String xpath = testStep.getKey();
			JavascriptExecutor js= (JavascriptExecutor) driver;
			Objects.requireNonNull(xpath, "XPATH Undefined");

			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(KeywordConstants.globalTimeout))
					.ignoring(NoSuchElementException.class)
					.ignoring(StaleElementReferenceException.class).ignoring(ElementNotVisibleException.class);

			boolean elementExists = wait.until(new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					boolean returnval = false;
					try {
						WebElement element = driver.findElement(By.xpath(testStep.getKey()));
						if (element != null && element.isDisplayed() && element.isEnabled()) {
							Select select = new Select(element);
							if (select.getOptions().size() > 0)
								returnval = true;
						}
						return returnval;
					} catch (Exception e) {
						return returnval;
					}
				}
			});

			if (elementExists == true) {
				element = getElement(testStep.getKey());
				
				js.executeScript("arguments[0].scrollIntoView(true);", element);
				Select select = new Select(element);
				String[] dropDownValues = testStep.getTestData().split(";");
				if (dropDownValues.length >= 1) {

					for (int i = 0; i < dropDownValues.length; i++) {
						if (element.isEnabled()) {
							String value = getVariableValue(runTimeMap, dropDownValues[i]);
							List<WebElement> elementList=select.getOptions();
							for (WebElement webElement : elementList)
							{
								js.executeScript("arguments[0].scrollIntoView(true);", webElement);
								if(webElement.getText().trim().equalsIgnoreCase(value))
								{
								select.selectByVisibleText(value);
								actualResult = true;
								testCaseLog.accept(testStep,testStep.getTestStepDescription() + " values - " + testStep.getTestInput1());
								}
							}
							
						} 
						else {
							throw new Exception("Dropdown is not enabled");
						}
					}
					
				} else {
					select.selectByVisibleText(dropDownValues[0]);
					actualResult = true;
					testCaseLog.accept(testStep,testStep.getTestStepDescription() + " Values Selected - " +testStep.getTestData());
				}
			}
		} catch (Exception e) {
			testCaseLog.accept(testStep,e.toString());
			this.exception = e.toString();
		}
		return actualResult;
	}
	
	@Override
	public boolean waitForPageToLoad()  {
		WaitMethods waitmethodClass;
		String loadWait = Optional.ofNullable(prop.getProperty("page.load.wait")).orElse("");
		if (loadWait.equals(""))
			waitmethodClass = new WaitMethods(driver, wait);
		else
			waitmethodClass = new WaitMethods(driver, wait, loadWait);
		waitmethodClass.waitForPageToLoad();
		return true;
		
	}
	
	/**
	 * The below method will try to find web element (any type) eg: will verify
	 * Text present on page
	 * 
	 * @param testStep
	 * @return
	 */
	public boolean verifyMessage(TestSteps testStep) {
		boolean actualResult = false;
		WebElement element = null;
		Wait<EventFiringWebDriver> elementWait;
		String searchValue = testStep.getTestData();
		elementWait = new FluentWait<EventFiringWebDriver>(driver)
				.withTimeout(Duration.ofSeconds(15))
				.pollingEvery(Duration.ofMillis(20))
				.ignoring(WebDriverException.class)
				.ignoring(NoSuchElementException.class)
				.ignoring(ElementNotVisibleException.class)
				.ignoring(StaleElementReferenceException.class);
		try {
//			searchValue = getVariableValue(this.runTimeMap, testStep.getTestData());
			String xpath = testStep.getKey();
			Objects.requireNonNull(xpath, "XPATH Undefined");
			if(xpath.contains("{0}"))
			{
				xpath = MessageFormat.format(xpath, searchValue);
			}	
				
			element = elementWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			String actualText = Optional.ofNullable(element.getText()).orElse(element.getAttribute("value"));
			System.out.println("element.getText="+element.getText());
			System.out.println("element.getAttribute(value)=" + element.getAttribute("value"));
			if (actualText.toLowerCase().contains(searchValue.toLowerCase())) {
				actualResult = true;
				Thread.sleep(2000);
				testCaseLog.accept(testStep,"Verified text is present " + testStep.getTestData());
			} else 
			{
				throw new Exception("Text not displayed. Expected text : " + searchValue + " Actual Text: " + actualText);
			}
			
		} catch (Exception e) {
			testCaseLog.accept(testStep, e.toString());
			this.exception = e.toString();
		}
		return actualResult;
	}

	public boolean enterRandomTextData(TestSteps testStep) {
			boolean randomDataEntered = false;
			PulseLib lib = new PulseLib(driver);
			String data="";
			try {
				    data = lib.getPulseRandomData(testStep.getTestInput1());
					randomDataEntered=enterRandomText(testStep, data);	
				
			} catch (Exception e) {
				testStep.setExecutionDetails(e.getMessage());
			}
			return randomDataEntered;
		}

	public boolean selectDate(TestSteps testStep) {
		boolean actualResult = false;
		try {
//			Thread.sleep(2000);
			StringTokenizer st = new StringTokenizer(testStep.getTestData(), "+|-", true);
			String refDate = st.nextToken();
			String arithmeticOperator = null;
			String daysModified = null;
			try {
				arithmeticOperator = st.nextToken();
				daysModified = st.nextToken();
			} catch (Exception e) {
			}

	String referenceDate = null;
			   if ("systemDate".equalsIgnoreCase(refDate)) {
                   LocalDate date = LocalDate.now();
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ENGLISH);
    referenceDate = date.format(formatter);
                                    }
			   else{
         referenceDate = this.configProp.getProperty(refDate);
                 if (referenceDate == null)
                    referenceDate = this.runTimeMap.get(refDate.toLowerCase());
                                     }

			
			String[] yearArr = referenceDate.split("-");
			int yearLenght = yearArr[yearArr.length - 1].length();
			DateTimeFormatter formatter = null;
			if (yearLenght == 2)
				formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ENGLISH);
			else if (yearLenght == 4)
				formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ENGLISH);
			LocalDate formattedDate = LocalDate.parse(referenceDate, formatter);
			LocalDate dateTobeSelected = null;
			if (arithmeticOperator != null && arithmeticOperator.equals("+"))
				dateTobeSelected = formattedDate.plusDays(Long.parseLong(daysModified));
			else if (arithmeticOperator != null && arithmeticOperator.equals("-"))
				dateTobeSelected = formattedDate.minusDays(Long.parseLong(daysModified));
			else
				dateTobeSelected = formattedDate;

			DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("d,MMM,yyyy");
			testStep.setTestData(dateTobeSelected.format(formatter2));
				               boolean retVal=false;
				              // PulseDateLib dateLib= new PulseDateLib(driver);
				               retVal=selectDateFromCalendar(testStep);
				               return retVal;
				        } catch (Exception e) {
				               e.printStackTrace();
				               this.exception = e.toString();
				               testCaseLog.accept(testStep, e.toString());
				        }
				        
				        System.out.println("Actual date passed: "+actualResult);
				        return actualResult;
			
					
					}

	/**
		 * This method is going to search in the table by getting the value from the
		 * dynamic data map by passing the key
		 * 
		 * @param testStep
		 * @return
		 */
		@Override
		public boolean clickOnDependentElement(TestSteps testStep) {
	
			boolean notexistsLoading=false;
	        try {
				Thread.sleep(500);
			} catch (InterruptedException e2) {
						}
			boolean actualResult = false;
			WebElement element = null;
			String searchValue = null;
			try {
				
				String[] testDataParams = testStep.getTestData().split(";");
				searchValue = getVariableValue(this.runTimeMap, testDataParams[0]);
				int searchValueColumn = Integer.parseInt(testDataParams[1]);
				int dependentValueColumn = Integer.parseInt(testDataParams[2]);
				String xpathForTable = testStep.getKey();
				Objects.requireNonNull(xpathForTable, "XPATH Undefined");
				WebElement tablename = getElement(xpathForTable);
	//			tablename = getElement(xpathForTable);
				JavascriptExecutor js = (JavascriptExecutor) driver;
	//			js.executeScript("arguments[0].scrollIntoView(true);", tablename);
				List<WebElement> tablerows = tablename.findElements(By.tagName("tr"));
				int rowCount = 1;
				try {
					WebElement thead = tablename.findElement(By.tagName("thead"));
					if (thead != null) {
						rowCount = 0;
					}
				} catch (Exception e1) {
	//				log.error(e1);
					e1.printStackTrace();
				}
	
				loopAllRows: for (WebElement row : tablerows) {
	
					List<WebElement> tablecolumns = row.findElements(By.tagName("td"));
					if (tablecolumns.size() == 0) {
						rowCount++;
						continue;
					}
	
					// System.out.println("Number of cells In Row " + row + " are "
					// + tablecolumns.size());
					String celltext = tablecolumns.get(searchValueColumn - 1).getText();
					if (celltext.toLowerCase().equals(searchValue.toLowerCase())) {
						WebElement clickElement = null;
						StringBuilder localXpath = new StringBuilder();
						try {
							localXpath.append(xpathForTable + "/tbody/tr[" + (rowCount) + "]/td[" + dependentValueColumn
									+ "]/a");
							localXpath.append("|" + xpathForTable + "/tbody/tr[" + (rowCount) + "]/td["
									+ dependentValueColumn + "]/span[1]/a");
							localXpath.append("|" + xpathForTable + "/tbody/tr[" + (rowCount) + "]/td["
									+ dependentValueColumn + "]/div/a");
							localXpath.append("|"+xpathForTable + "/tbody/tr[" + (rowCount) + "]/td[" + dependentValueColumn
									+ "]/input[@type='checkbox']");
							clickElement = driver.findElement(By.xpath(localXpath.toString()));
						} catch (Exception e) {
							e.printStackTrace();
						}
						clickElement = driver.findElement(By.xpath(localXpath.toString()));
	
						js = (JavascriptExecutor) driver;
	//					js.executeScript("arguments[0].scrollIntoView(true);", clickElement);
						js.executeScript("arguments[0].click();", clickElement);
						// clickElement.click();
						actualResult = true;
						testCaseLog.accept(testStep, testStep.getTestStepDescription() + " :"+ searchValue);
						break loopAllRows;
					}
					rowCount++;
				}
				if (!actualResult)
					throw new Exception(searchValue + " : Not Present in Grid");
			} catch (Exception e) {
				this.exception = e.toString();
				testCaseLog.accept(testStep, e.toString());
			}
	
			return actualResult;
		}

	/*
		 * The method used to login with an System Admin
		 * 
		 */
		
	public boolean login(TestSteps testStep) 
	{
	boolean actualResult = false;
	WebElement element = null;

	
    try {
            user = configProp.getProperty("" + testStep.getTestData() + ".usn");
			password = configProp.getProperty("" + testStep.getTestData() + ".pwd");
		getElement(prop.getProperty("logininfo.username.textbox")).sendKeys(user);
		getElement(prop.getProperty("logininfo.next.button")).click();
		getElement(prop.getProperty("logininfo.password.textbox")).sendKeys(password);
		getElement(prop.getProperty("logininfo.signin.button")).click();
		 try {
		element = driver.findElement(By.xpath("//a[normalize-space(text())='I Agree']"));
		 if (element != null || element.isDisplayed()) {
				element.click();
			}
		 }
		 catch(NoSuchElementException|NullPointerException ex )
		 {
			 
		 }
      
		actualResult = true;
		testCaseLog.accept(testStep, testStep.getTestStepDescription() + " User id=" + user);
	} catch (Exception e) {
		this.exception = e.getClass().getSimpleName();
		testCaseLog.accept(testStep, e.toString());
	}
	

	return actualResult;
		
}

	/*public boolean executeSQLScript(TestSteps testStep){
			String queryName = null;
			String[] queryParams = null;
			String query = null;
			BankDBConnection db = null;
			Connection conn = null;
			boolean actualResult = false;
			try {
				String testData = testStep.getTestData();
				String[] queryData = testData.split(";");
				if(queryData.length==2){
					queryName = queryData[0].split("-")[1];
					queryParams = queryData[1].split("-")[1].split(",");
				}else if(queryData.length==1){
					queryName = queryData[0].split("-")[1];
				}
				
				
				//Get Query from property file
				if(queryName==null){
					log.error("Query Name Undefined");
					throw new Exception("Query Name Undefined");
				}else{
					Properties sqlProp = new Properties();
					sqlProp.load(new FileInputStream(KeywordFolders.KEYWORD_BASE_PATH + "/AXIS/Repository/SQLScripts.properties"));
					query=sqlProp.getProperty(queryName);
					if(query==null){
						log.error("Query Undefined");
						throw new Exception("Query Undefined");
					}
				}
				
				//Establish connection with DB and execute query
				db= new BankDBConnection();
				conn = db.getConnection(this.projectName);
				if(conn==null){
					log.error("DB Connection Failure");
					throw new Exception("DB Connection Failure");
				}else{
					PreparedStatement stmt = conn.prepareStatement(query);
					if(queryParams!=null){
						int i=1;
						for (String param : queryParams) {
							if (param.startsWith("[") && param.endsWith("]")) {
								String propKey = StringUtils.substringBetween(param, "[", "]").trim();
								param = this.configProp.getProperty(propKey);
							}
							param = actionsLib.getVariableValue(runTimeMap, param);
							if(param.equals("BG"))
								param = param + this.dateFormat.getDateInHHmmssFormat();
							stmt.setString(i, param);
							i++;
						}
					}
					stmt.execute();
					conn.commit();
					stmt.close();
					testStep.setActualResult("PASS");
					actualResult = true;
				}
				
			} catch (Exception e) {
				this.exception = e.toString();
				testStep.setActualResult(this.exception);
				log.error("Error while clicking web element " + testStep.getKey() + " " + this.exception);
			}finally{
				if(conn!=null)
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
			}
			
			return actualResult;
		}*/
		
		
		public boolean executeSQLScript(TestSteps testStep){
		    String queryName = null;
		    String[] queryParams = null;
		    String query = null;
		    BankDBConnection db = null;
		    Connection conn = null;
		    boolean actualResult = false;
		    try {	    	
		           String testData = testStep.getTestData();
		           String[] queryData = testData.split(";");
		           if(queryData.length==2){
		                 queryName = queryData[0].split("-")[1];
		                 queryParams = queryData[1].split("-")[1].split(",");
		           }else if(queryData.length==1){
		                 queryName = queryData[0].split("-")[1];
		           }
		       	           
		           //Get Query from property file
		           if(queryName==null){
		                 throw new Exception("Query Name Undefined");
		           }else{
		                 Properties sqlProp = new Properties();
		                 sqlProp.load(new FileInputStream(KeywordFolders.KEYWORD_BASE_PATH + "/PULSE/Repository/SQLScripts.properties"));
		                 query=sqlProp.getProperty(queryName);
		                 if(query==null){
		                        throw new Exception("Query Undefined");
		                 }
		           }
		           
		           //Establish connection with DB and execute query
		           db= new BankDBConnection();
		           conn = db.getConnection(this.projectName);
		           if(conn==null){
		                 throw new Exception("DB Connection Failure");
		           }else{
		                 PreparedStatement stmt = conn.prepareStatement(query);
		                 if(queryParams!=null){
		                        int i=1;
		                        for (String param : queryParams) {
		                        	String var = null;
		                               if (param.startsWith("[") && param.endsWith("]")) {	                            	 
		                                      String propKey = StringUtils.substringBetween(param, "[", "]").trim();
		                                      param = this.configProp.getProperty(propKey);
		                               }
		                            // need to test the changes in other scripts for the below line
		                               String param1 = getVariableValue(runTimeMap, param);
		                                
	//	                               if(param.equalsIgnoreCase("BG")) {
	//	                                      param = param + this.dateFormat.getDateInHHmmssFormat();
	//	                                      this.runTimeMap.put("bg", param);
	//	                               }
	//	                               if(param.equalsIgnoreCase("LC")) {
	//	                            	      param = param + this.dateFormat.getDateInHHmmssFormat();
	//	                                      this.runTimeMap.put("lc", param);
	//	                               }
	//	                               if(param.equalsIgnoreCase("FWC")) {
	//	                                      param = param + this.dateFormat.getDateInHHmmssFormat();
	//	                                      this.runTimeMap.put("fwc", param);
	//	                               }
		                                if(param.equalsIgnoreCase(param1)){
		                                	  var = param + this.dateFormat.getDateInHHmmssFormat();
		                                      this.runTimeMap.put(param.toLowerCase(), var);
		                               }else{
		                            	   var = param1;
		                               }
		                              	                               
		                               stmt.setString(i, var);
		                               i++;
		                        }
		                 }
		                
		                 stmt.execute();
		                int a =  stmt.getUpdateCount();
//		                System.out.println(a);
		                 conn.commit();
		                 stmt.close();
		                 actualResult = true;
		                 testCaseLog.accept(testStep, testStep.getTestStepDescription());
		           }
		           
		    } catch (Exception e) {
		           this.exception = e.toString();
		           testCaseLog.accept(testStep, e.toString());
		    }finally{
		           if(conn!=null)
		                 try {
		                        conn.close();
		                 } catch (SQLException e) {
		                        e.printStackTrace();
		                 }
		    }
		    
		    return actualResult;
		}

		/**
		 * This keyword is used to verify if the expected options are present in the
		 * dropdown in the application are not. More than one option can be verified
		 * by entering all the options to be verified separated by semicolon
		 * 
		 * @param testStep
		 * @return
		 */
		public boolean verifyDropDownValues(TestSteps testStep) {
			String[] dropDownValues = null;
		
			boolean actualResult = false;
			WebElement element = null;
			boolean contains = false;
			try {
					element = getElement(testStep.getKey());
					Select select = new Select(element);
					List<WebElement> options = select.getOptions();
					if (testStep.getTestData().startsWith("{") && testStep.getTestData().endsWith("}")) {
						dropDownValues = this.runTimeMap
								.get(StringUtils.substringBetween(testStep.getTestData(), "{", "}").toLowerCase())
								.split(";");
					} else
						dropDownValues = testStep.getTestData().split(";");
					List<String> dropDownValuesExpList = Arrays.asList(dropDownValues);
		
					List<String> dropDownValuesActList = new ArrayList<>();
					for (WebElement option : options) {
						dropDownValuesActList.add(option.getText());
					}
					if (dropDownValuesActList.containsAll(dropDownValuesExpList))
					{
						actualResult = true;
					    testCaseLog.accept(testStep,testStep.getTestStepDescription() + " values-" + testStep.getTestInput1());
					}
					 else
					 {
						throw new Exception(" - Expected: " + dropDownValuesExpList + " Actual: " + dropDownValuesActList);
					 }
				
			} catch (Exception e) {
				e.printStackTrace();
				testCaseLog.accept(testStep,e.toString());
				this.exception = e.toString();
			}
		
			return actualResult;
		}

		/**
		 * This keyword is used to verify the given values in the test_data column
		 * are not present in the drop down. Only one value can be verified or
		 * multiple values can be verified. If the multiple values have to be
		 * verified then they should be separated by semicolon
		 * 
		 * @param testStep
		 * @return
		 */
		public boolean selectDateFromCalendar(TestSteps testStep) {
			boolean actualResult = false;
			WebElement element = null;
			try {
				String[] testDataParams = testStep.getTestData().split(",");
				String day = testDataParams[0];
				String paraMonth = testDataParams[1];
				String expMonth=paraMonth;
				String expYear = testDataParams[2];
				Select dropDown = null;
				element = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']"));
				dropDown = new Select(element);
				dropDown.selectByVisibleText(expYear);
				Select monthdropDown = null;
				element = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']"));
				monthdropDown = new Select(element);
				monthdropDown.selectByVisibleText(expMonth);
				List<WebElement> dateElements = driver.findElements(By.xpath("//div[@id=\"ui-datepicker-div\"]/table/tbody/tr/td/a[text()='" + day + "']"));
				for (WebElement webElement : dateElements) {
//					System.out.println(webElement.getText());
					for (int k = 0; k < 3; k++) {
						try {

							if (webElement.isDisplayed()) {
								webElement.click();
								testCaseLog.accept(testStep, testStep.getTestStepDescription());
								break;
							}
							Thread.sleep(50);
						} catch (StaleElementReferenceException e) {
					e.printStackTrace();
//					System.out.println("Calendar : StaleElementReferenceException");
				} 
			}
				}

				actualResult = true;
			} catch (Exception e) {
				e.printStackTrace();
				this.exception = e.toString();
				testCaseLog.accept(testStep, e.toString());
			}

			return actualResult;
		}
		
		public boolean captureRunTimeValue(TestSteps testStep) {

			WebElement element = null;
			boolean actualResult = false;
			String dataCaptured = "";
			try {
				String key = testStep.getTestData().toLowerCase();
				if (key.equals("")) {
					key = "notdefined";
				}
				Date d = new Date();
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat simpleDate = new SimpleDateFormat("M/d/yyyy hh:mm:ss");
				if (key.equalsIgnoreCase("start time")) {
					cal.add(Calendar.SECOND, 27);
					/*
					 * cal.add(Calendar.HOUR, 10); cal.add(Calendar.MINUTE, 30);
					 * cal.add(Calendar.SECOND, 16);
					 */

					String startTime = simpleDate.format(cal.getTime());
					System.out.println("StartTime" + startTime);
					testStep.setActualResult("Start Time Captured: " + startTime);
					this.runTimeMap.put(key.toLowerCase(), startTime);
					logInfo(testStep.getTestStepDescription() + " :" + startTime);
					testCaseLog.accept(testStep,"Captured value from element = " + startTime);
					return actualResult = true;
				} else if (key.equalsIgnoreCase("stop time")) {
					cal.add(Calendar.SECOND, 27);
					/*
					 * cal.add(Calendar.HOUR, 10); cal.add(Calendar.MINUTE, 30);
					 * cal.add(Calendar.SECOND, 16);
					 */

					String stopTime = simpleDate.format(cal.getTime());
					System.out.println(stopTime);
					this.runTimeMap.put(key.toLowerCase(), stopTime);
//					save to variable method needs to be changed for the below case as var name is in test data
//					savetoVariable(testStep, stopTime);
					testCaseLog.accept(testStep,"Captured value from element = " + stopTime);
					return actualResult = true;
				}

					// Thread.sleep(2000);
					element = getElementToVerifyDisable(testStep.getKey());
					if (!(element == null)) {
						dataCaptured = Optional.ofNullable(element.getAttribute("value")).orElse(element.getText());
						if (!key.equals("") || !testStep.getSaveToVariable().equals(""))
							this.runTimeMap.put(key, dataCaptured);
						System.out.println("Data Captured: " + dataCaptured);
						actualResult = true;
						testCaseLog.accept(testStep,"Captured value from element = " + dataCaptured);
//						System.out.println("Successfully verified Text present :  " + dataCaptured);
						// log.info("Successfully verified captured : " + key + ": "
						// + dataCaptured);
						
					} else {
						throw new Exception("Element not present");
					}
				

				if (testStep.getSaveToVariable() != null || !testStep.getSaveToVariable().isEmpty()) {
					if (testStep.getSaveToVariable().startsWith("{")) {
						String keyname = StringUtils.substringBetween(testStep.getSaveToVariable(), "{", "}");
						runTimeMap.put(keyname, dataCaptured);
					}
				}
			} catch (Exception e) {
				this.exception = e.toString();
				testCaseLog.accept(testStep, e.toString());
			}
			return actualResult;
		}

		
		public boolean verifyListValuesPresent(TestSteps testStep) {
			boolean actualResult = false;
			WebElement element = null;
			boolean contains = false;
			try {
					
//					element = getElement(testStep.getKey());
//					Select select = new Select(element);
					String xpath = testStep.getKey();
					List<WebElement> options = driver.findElements(By.xpath(xpath));
					//System.out.println("MenuOptions element =" + options);
					String[] dropDownValues = testStep.getTestData().split(";");
					List<String> dropDownValuesExpList = Arrays.asList(dropDownValues);
					List<String> dropDownValuesActList = new ArrayList<>();
					for (WebElement option : options) {
						//System.out.println(option.getText());
						dropDownValuesActList.add(option.getText());
						
					}
					if (dropDownValuesActList.containsAll(dropDownValuesExpList))
						{
						actualResult = true;
						testCaseLog.accept(testStep,testStep.getTestStepDescription() + "   :    Searched ELement is present in the menu list     -      Actual values displayed: " + dropDownValuesActList);
						}		
					else
					{
						throw new Exception("       :       Element not present in the list     -   Expected:" + dropDownValuesExpList + " Actual: " + dropDownValuesActList);	
					} 
				 
			} catch (Exception e) {
				testCaseLog.accept(testStep,e.toString());
				this.exception = e.toString();
			}

			return actualResult;
		}
		
		public boolean customizedDateToBeSelected(TestSteps testStep) {
	        boolean actualResult = false;
	        try {
	               StringTokenizer st = new StringTokenizer(testStep.getTestData(), "+|-", true);
	               String refDate = st.nextToken();
	               String arithmeticOperator=null;
	               String daysModified=null;
	               try {
	                     arithmeticOperator = st.nextToken();
	                     daysModified = st.nextToken();
	               } catch (Exception e) {
	               }

	                String referenceDate = null;
	                                                               if ("systemDate".equalsIgnoreCase(refDate)) {
	                                                                                LocalDate date = LocalDate.now();
	                                                                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MM-yyyy", Locale.ENGLISH);
	                                                                                referenceDate = date.format(formatter);
	                                                               }else{
	                                                                                referenceDate = this.configProp.getProperty(refDate);
	                                            if (referenceDate == null)
	                                               referenceDate = this.runTimeMap.get(refDate.toLowerCase());
	                                                                }
	               if(referenceDate==null)
	                  throw new Exception("Parameter for custom date is missing in test script");
	               
	               DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MM-yyyy", Locale.ENGLISH);
	               LocalDate formattedDate = LocalDate.parse(referenceDate, formatter);
	               LocalDate dateTobeSelected = null;
	               if (arithmeticOperator!=null&&arithmeticOperator.equals("+"))
	                     dateTobeSelected = formattedDate.plusDays(Long.parseLong(daysModified));
	               else if (arithmeticOperator!=null&&arithmeticOperator.equals("-"))
	                     dateTobeSelected = formattedDate.minusDays(Long.parseLong(daysModified));
	               else
	                     dateTobeSelected = formattedDate;

	               DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("d,MMM,yyyy");
	               testStep.setTestData(dateTobeSelected.format(formatter2));
	               return this.selectDateFromCalendar(testStep);
	        } catch (Exception e) {
	               e.printStackTrace();
	               this.exception = e.toString();
	               testStep.setActualResult(this.exception);
	               log.error("Error Unable to assert Text " + testStep.getKey() + " " + this.exception);
	        }
	        
	        System.out.println("Actual date passed: "+actualResult);
	        return actualResult;
	       }
		
		public boolean verifyListValuesNotPresent(TestSteps testStep) {
			boolean actualResult = false;
			WebElement element = null;
			boolean contains = false;
			try {
					
					String xpath = testStep.getKey();
					List<WebElement> options = driver.findElements(By.xpath(xpath));		
					//System.out.println("MenuOptions element =" + options);
					String[] dropDownValues = testStep.getTestData().split(";");
					List<String> dropDownValuesExpList = Arrays.asList(dropDownValues);
					List<String> dropDownValuesActList = new ArrayList<>();
					for (WebElement option : options) {
						//System.out.println(option.getText());
						dropDownValuesActList.add(option.getText());
						
					}
					if (dropDownValuesActList.containsAll(dropDownValuesExpList))
							throw new Exception(" - Expected:" + dropDownValuesExpList + " Actual: " + dropDownValuesActList);
					else {
						actualResult = true;
						testCaseLog.accept(testStep,testStep.getTestStepDescription() + "   :    Searched ELement is not present     -      Actual values displayed: " + dropDownValuesActList);
					} 
				 
			} catch (Exception e) {
				testCaseLog.accept(testStep,e.toString());
				this.exception = e.toString();
			}

			return actualResult;
		}
		
		public boolean isDisabled(TestSteps testStep) {
			boolean actualResult = false;
			WebElement element = null;
			String actualDisabledAttributeValue = null;
			
			try {
					
					element = getElementToVerifyDisable(testStep.getKey()); 
					actualDisabledAttributeValue = element.getAttribute("disabled");
					
					if (actualDisabledAttributeValue.contains("true") ) {
						testStep.setAssertExpected("disabled");
						testStep.setAssertActual("disabled");
						actualResult = true;
						testCaseLog.accept(testStep, testStep.getTestStepDescription());
					} else
					{
						testStep.setAssertActual("enabled");
						throw new Exception("Element is not disabled");
					}
			} catch (Exception e) {		
				testCaseLog.accept(testStep, e.toString());
				this.exception = e.toString();
				logError(testStep.getTestStepDescription() + "- Test Data:-" + ("".equals(testStep.getTestData().trim())?EMPTY:testStep.getTestData()) + "- Exception:" + this.exception);
			}
		
			return actualResult;
		}

		public boolean clearText(TestSteps testStep) {
			boolean actualResult = false;
			WebElement element = null;
			try {
				element = getElement(testStep.getKey());
				element.click();
				element.sendKeys(Keys.BACK_SPACE,Keys.BACK_SPACE,Keys.BACK_SPACE);
				
				if(element.getText()=="" || element.getText()==" " || element.getText()==null || element.getText().isEmpty()){
						actualResult = true;
						testCaseLog.accept(testStep, testStep.getTestStepDescription());
		
					} else {
						throw new Exception(testStep.getTestStepDescription() + "TextBox not cleared");
		
					}
		
			} catch (Exception e) {
				this.exception = e.getClass().getName() + " " + e.getMessage();
				testCaseLog.accept(testStep, e.toString());
			}
			return actualResult;
		
		}
	
		
		public boolean verifyDropDownValuesNotPresent(TestSteps testStep) {
			boolean actualResult = false;
			WebElement element = null;
			boolean contains = false;
			try {
					element = getElement(testStep.getKey());
					Select select = new Select(element);
					List<WebElement> options = select.getOptions();
					String[] dropDownValues = testStep.getTestData().split(";");
					List<String> dropDownValuesExpList = Arrays.asList(dropDownValues);
					List<String> dropDownValuesActList = new ArrayList<>();
					for (WebElement option : options) {
						dropDownValuesActList.add(option.getText());
					}
					if (dropDownValuesActList.containsAll(dropDownValuesExpList))
							throw new Exception(" - Expected:" + dropDownValuesExpList + " Actual: " + dropDownValuesActList);
					else {
						actualResult = true;
						testCaseLog.accept(testStep,testStep.getTestStepDescription() + " Actual values displayed: " + dropDownValuesActList);
					} 
				 
			} catch (Exception e) {
				testCaseLog.accept(testStep,e.toString());
				this.exception = e.toString();
			}
		
			return actualResult;
		}

		public boolean joinString(TestSteps testStep) {
			boolean actualResult=false;
			String finalString="";
			
			try {
		
				String[] testData=testStep.getTestInput1().split(";");
				if(testData.length>1)
				{
					for (int i = 0; i < testData.length; i++) {
						
						finalString=finalString.concat(testData[i].trim());
						
					}
					System.out.println(testStep.getSaveToVariable());
					savetoVariable(testStep, finalString);
					actualResult=true;
					testCaseLog.accept(testStep, "Joined string value=" + finalString);
				}
				else
				{
					testCaseLog.accept(testStep, "please provide strings to be concatinated seperated by semicolon ';'");
				}
				
			} catch (Exception e) {
				testCaseLog.accept(testStep,  e.toString());
			}
		
			return actualResult;
		}
		
		
//		public boolean verifyRowValues(TestSteps testStep) {
//			String actualValues="";
//			String expectedValues="";
//			boolean actualResult = false;
//			try {
//				String[] colNameArray = testStep.getTestData().split(";");
//				String xpath = testStep.getKey();
//				System.out.println("Test Data-" + testStep.getTestInput1());
//				Thread.sleep(800);
//				if (xpath != null && xpath != "" && !xpath.isEmpty()) {
//					for (int k = 0; k < 3; k++) {
//						WebElement tablename = getElement(xpath);
//						System.out.println("table element ="+tablename);
//						try {
//							
//							ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
//								int rowFound=0;
//						
//								public Boolean apply(WebDriver driver) {
//									
//									List<WebElement> tablerows = tablename.findElements(By.tagName("tr"));
//									if(tablerows.size()==2)
//									{
//										   rowFound+=1;
//										  if(rowFound==2)
//											   return true;
//										  else 
//											  return false;
//									}
//										else 
//										{
//											return false;
//										}
//											
//										
//								}
//							};
//							Wait<WebDriver> wait2 = new FluentWait<WebDriver>(driver)
//									.withTimeout(Duration.ofSeconds(30))
//									.pollingEvery(Duration.ofMillis(2000))
//									.ignoring(NoSuchElementException.class)
//									.ignoring(WebDriverException.class)
//									.ignoring(StaleElementReferenceException.class);
//							boolean twoRowsFound = wait2.until(expectation);
//							if(twoRowsFound == true)
//							{
//							List<WebElement> tablerows = tablename.findElements(By.tagName("tr"));
//							WebElement row=tablerows.get(1);
//								List<WebElement> tableColumns = row.findElements(By.tagName("td"));
//								if (tableColumns.size() == 0)
//									continue;
//								int i = 0;
//								int failCount = 0;
//
//								for (String columnValue : colNameArray) {
//									columnValue.trim();
//									if (!columnValue.equalsIgnoreCase("ignore")) {
//										String searchValue = columnValue;
//											System.out.print(tableColumns.get(i).getText() + ";");
//											actualValues=expectedValues.concat(tableColumns.get(i).getText().toLowerCase().trim());
//											actualValues=actualValues.concat(";");
//											expectedValues=actualValues.concat(searchValue.toLowerCase().trim());
//											expectedValues= actualValues.concat(";");
//											failCount = tableColumns.get(i).getText().toLowerCase().trim()
//													.contains(searchValue.toLowerCase().trim()) ? failCount : failCount + 1;
//										i++;
//									}
//
//									else {
//										i++;
//										System.out.print("ignore;");
//									}
//								}
//								if (failCount == 0) {
//									actualResult = true;
//									testCaseLog.accept(testStep, testStep.getTestStepDescription() + "Test Data-" + testStep.getTestInput1());
//									break;
//								}
//							
//
//							if (!actualResult)
//								throw new Exception("Data Mismatch");
//							break;
//						}
//						} catch (StaleElementReferenceException staleExp) {
//							System.out.println("Verify Row Values1: Stale Element Exception");
//							Thread.sleep(3000);
//						}
//						k++;
//					}
//				} else {
//					throw new Exception("XPATH Undefined");
//				}
//			} catch (Exception e) {
//				testCaseLog.accept(testStep, e.toString());
//				this.exception = e.toString();
//			}
//
//			return actualResult;
//		}
		
		public boolean clickElementByScrolling(WebElement element) throws InterruptedException{
	        boolean elementClicked = false;
	        JavascriptExecutor executor= null;
	        try {
	        executor = (JavascriptExecutor) driver;
	        if (element == null) {
	        	return false;
	        }
			executor.executeScript("arguments[0].scrollIntoView(true);", element);
	        Thread.sleep(500);
	     
	            element.click();
	            elementClicked = true;
	         }catch(Exception ex) {
//	            System.out.println("Scroll Up by 200 pixels");
	            executor.executeScript("window.scrollBy(0, -200);", element);
	            Thread.sleep(500);
	            element.click();
	            elementClicked = true;
	            /*try {
	                element.click();
	                elementClicked = true;
	            } catch (Exception e) {
	                System.out.println("Scroll Down by 400 pixels");
	                executor.executeScript("window.scrollBy(0, 400);", element);
	                Thread.sleep(2000);
	                element.click();
	                elementClicked = true;
	            }*/
	         }
	        return elementClicked;
	    }
		@Override
		public boolean verifyRowValues(TestSteps testStep) {

			boolean actualResult = false;
			try {
				waitForLoadingImg();
				String expectedValues="";
				String actualValues="";
				String[] colNameArray = testStep.getTestData().split(";");
				String xpath = testStep.getKey();
				System.out.println("Test Data-" + testStep.getTestInput1());
				Objects.requireNonNull(xpath, "XPATH Undefined");
				Thread.sleep(1500);
					for (int k = 0; k < 3; k++) {
						WebElement tablename = getElement(xpath);
						try {
							List<WebElement> tablerows = tablename.findElements(By.tagName("tr"));
							for (WebElement row : tablerows) {
								List<WebElement> tableColumns = row.findElements(By.tagName("td"));
								if (tableColumns.size() == 0)
									continue;
								int i = 0;
								int failCount = 0;
								
								for (String columnValue : colNameArray) {
									columnValue.trim();
									if (!columnValue.equalsIgnoreCase("ignore")) {
										String searchValue = columnValue;
											System.out.print(tableColumns.get(i).getText() + ";");
											actualValues=expectedValues.concat(tableColumns.get(i).getText().toLowerCase().trim());
											actualValues=actualValues.concat(";");
											expectedValues=actualValues.concat(searchValue.toLowerCase().trim());
											expectedValues= actualValues.concat(";");
											failCount = tableColumns.get(i).getText().toLowerCase().trim()
													.contains(searchValue.toLowerCase().trim()) ? failCount : failCount + 1;
										i++;
									}

									else {
										i++;
										System.out.print("ignore;");
									}
								}
								testStep.setAssertExpected(actualValues);
								testStep.setAssertActual(expectedValues);
								if (failCount == 0) {
									actualResult = true;
									testCaseLog.accept(testStep, testStep.getTestStepDescription() + "Test Data-" + testStep.getTestInput1());
									break;
								}
							}
							
							if (!actualResult)
								throw new Exception("Data Mismatch");
							break;
						} catch (StaleElementReferenceException staleExp) {
							staleExp.printStackTrace();
//							System.out.println("Verify Row Values1: Stale Element Exception");
//							Thread.sleep(3000);
						}
						k++;
					}
				 
//			}
			}catch (Exception e) {
				testCaseLog.accept(testStep, e.toString());
				this.exception = e.toString();
			}

			return actualResult;
		}

		public boolean logOut(TestSteps testStep) {
				boolean actualResult = false;
				try {
					switchToParentFrame(testStep);
				    WebElement usericon = getElement(prop.getProperty("common.usericon"));
				    usericon.click();
				    Thread.sleep(1000);
					WebElement signOut = getElement(prop.getProperty("uat.signout.button"));
					signOut.click();
					System.out.println("Logged Out");
					actualResult = true;
					
				} catch (Exception e) {
					// e.printStackTrace();
				}
				return actualResult;
			}		
		
		public boolean clickElementByScrolling(TestSteps testStep) {
			boolean actualResult = false;
			WebElement element = null;
			String xpath = testStep.getKey();
			try {
				if(xpath.contains("{0}"))
				{
					xpath = MessageFormat.format(xpath, testStep.getTestData());
				}
				element = getElementPresent(xpath);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
		        executor.executeScript("arguments[0].scrollIntoView(true);", element);
				element.click();
				actualResult = true;
				testCaseLog.accept(testStep,testStep.getTestStepDescription());
			} 
		
			catch(ElementClickInterceptedException ex)
			{
				testCaseLog.accept(testStep,"Another element overlapped the element you are tying to click.\n" + ex.getMessage());
				try {
					clickElementByScrolling(element);
				} catch (InterruptedException e1) {
					
					e1.printStackTrace();
				}
			}
			catch (Exception e) {
				this.exception = e.toString();
				testCaseLog.accept(testStep,e.getMessage());
				try {
					clickElementByScrolling(element);
				} catch (InterruptedException e1) {
									e1.printStackTrace();
				}
				
			}
			return actualResult;
		}
		public boolean selectMonth(TestSteps testStep) {
			
			WebElement element = null;
			String referenceDate = null;
			boolean actualResult = false;
			
			try {
				
				
				LocalDate currentDate = LocalDate.now();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ENGLISH);
				referenceDate = currentDate.format(formatter);
				
				String[] dateScheduled = testStep.getTestData().split("/");
				int month1 = Integer.parseInt(dateScheduled[0]);
				
				String[] formattedDate2 = referenceDate.split("-");
				int month2 = Integer.parseInt(formattedDate2[1]);
				
				if (month1 < month2) {
					
					element = driver.findElement(By.xpath("//button[text()='Today']//following::button[@aria-label='prev']"));
					Thread.sleep(1000);
					element.click();
					
				}  
				else if(month1>month2) {
					
				element = driver.findElement(By.xpath("//button[text()='Today']//following::button[@aria-label='next']"));
				Thread.sleep(1000);
				element.click();
				
				}
				
				else
					System.out.println("Sessions scheduled in the same month");

				actualResult = true;
			} catch (Exception e) {
				e.printStackTrace();
				this.exception = e.toString();
				testCaseLog.accept(testStep, e.toString());
			}

			return actualResult;
		}	
           public WebElement getElementPresent(String xpath) 
		{
		       
		       Objects.requireNonNull(xpath, "Xpath Undefined");
		       actionsLib.waitForPageToLoad();
		       WebElement visibleElement = null;
		       WebElement returnElement=null;
		       WebElement clickableElement=null;
		       WebElement element=null;
		       try {
		                      clickableElement=this.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		       } 
		       catch (Exception e1) {
		       }
		       
		       if (clickableElement == null )
		       {
		              try
		              {
		                    Thread.sleep(300);
		                    element = driver.findElement(By.xpath(xpath));
		              }
		              catch(StaleElementReferenceException ex)
		              {
		                    try {
		                           Thread.sleep(1000);
		                           element = driver.findElement(By.xpath(xpath));
		                           return element;
		                    } catch (InterruptedException e) {}
		              }
		              catch (InterruptedException e){}
		              System.out.println("Element is not clickable");
		              
		       }
		
		       if(clickableElement != null)
		              returnElement=clickableElement;
		       else
		              returnElement = element;
		       return returnElement;
		       
		}

		/**
			 * This method is used to open the browser in maximized mode and navigate to
			 * the respective URL mentioned
			 * 
			 * @param testStep
			 * @return
			 */
			public boolean launchApplication(TestSteps testStep) {
				boolean actualResult = false;
			    String url = configProp.getProperty("url");
			    WebElement element = null;
			    
				try
				{
					if (testStep.getTestData() == null || testStep.getTestData() == "") {
						try {
							driver.get(url);
							element = getElement(prop.getProperty("loginpage.pulse.text"));
							if (element == null) {
			                    System.out.println("Waiting for 5 Minutes");
							    TimeUnit.MINUTES.sleep(5);
							    driver.navigate().refresh();
							   element =  getElement(prop.getProperty("loginpage.pulse.text"));
							}
					}
						catch (WebDriverException e) {
							if (element == null) {
		                    System.out.println("Waiting for 5 Minutes");
		                    TimeUnit.MINUTES.sleep(5);
						    driver.navigate().refresh();
						   element =  getElement(prop.getProperty("loginpage.pulse.text"));	
					}
						}
						
						if (this.browser.equalsIgnoreCase("iexplorer")) {
							try {
								clickContinueLinkinIE(globalConfigProp.getProperty("windows.version"));
							} catch (Exception e2) {
								e2.printStackTrace();
							}
						}
						
						if (element != null) {
							actionsLib.waitForPageToLoad();
							this.parentWindow = driver.getWindowHandle();
							actualResult = true;
						    testCaseLog.accept(testStep,"Navigated to URL - " + url);
		//				logInfo(testStep.getTestStepDescription() + "- Test Data:-" + url==null?EMPTY:url);
					}
						}
				}
				 catch (Exception e) {
					testCaseLog.accept(testStep,"Not able to navigated to URL - " + url + "   " + e.getMessage());
					this.exception = e.toString();
						}
				return actualResult;
			}
			public boolean clearTextValue(TestSteps testStep) {
				boolean actualResult = false;
				WebElement element = null;
				try {
					element = getElement(testStep.getKey());
					element.clear();
					
					if(element.getText()=="" || element.getText()==" " || element.getText()==null || element.getText().isEmpty()){
							actualResult = true;
							testCaseLog.accept(testStep, testStep.getTestStepDescription());

						} else {
							throw new Exception(testStep.getTestStepDescription() + "TextBox not cleared");

						}

				} catch (Exception e) {
					this.exception = e.getClass().getName() + " " + e.getMessage();
					testCaseLog.accept(testStep, e.toString());
				}
				return actualResult;

			}

			/**
			 * Enters the given data in the 'test_data' column in the test script.
			 * Before entering the given data it will clear the text if present from the
			 * text box in the application
			 * 
			 * @param testStep
			 * @return
			 */
			
			public boolean search(TestSteps testStep) {
			    boolean actualResult = false;
			    WebElement element = null;
			    try {
			    	
			        element = getElement(testStep.getKey());
			        element.clear();
			        Thread.sleep(1500);
			        String testData= testStep.getTestData();
			        for(int i=0;i<testData.length();i++){
			        	char a=testData.charAt(i);
			        	Thread.sleep(140);
			        	element.sendKeys(Keys.chord(""+a+""));
			        }
			        
			        savetoVariable(testStep, testStep.getTestData());
			        actualResult = true;            
			        testCaseLog.accept(testStep, "Entered Text " + testStep.getTestData());
			
			
			    } catch (Exception e) {
			        e.printStackTrace();
			        testCaseLog.accept(testStep, e.toString());
			        this.exception = e.toString();
			    }
			    return actualResult;
			
			}
			

			public boolean joinStringWithSpaces(TestSteps testStep) {
				boolean actualResult=false;
				String finalString="";
				
				try {
			
					String[] testData=testStep.getTestInput1().split(";");
					if(testData.length>1)
					{
						for (int i = 0; i < testData.length; i++) {
							
							finalString=finalString.concat(testData[i]);
							
						}
						System.out.println(testStep.getSaveToVariable());
						savetoVariable(testStep, finalString);
						actualResult=true;
						testCaseLog.accept(testStep, "Joined string value=" + finalString);
					}
					else
					{
						testCaseLog.accept(testStep, "please provide strings to be concatinated seperated by semicolon ';'");
					}
					
				} catch (Exception e) {
					testCaseLog.accept(testStep,  e.toString());
				}
			
				return actualResult;
			}
			
}


