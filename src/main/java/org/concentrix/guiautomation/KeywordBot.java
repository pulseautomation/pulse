package org.concentrix.guiautomation;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.concentrix.guiautomation.utils.PropertiesLib;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import keyword.constants.KeywordConstants;
public class KeywordBot {
	
	public static void main( String[] args ) throws Exception
	    {
	    
		PropertiesLib globalConfig = new PropertiesLib("KeywordArtifacts/globalConfiguration.properties");
		String projectName = globalConfig.getProperty("project");
		globalConfig.closePropertyFile();
		System.out.println(projectName.toLowerCase()+ "_trigger");
		PropertiesLib prop = new PropertiesLib(KeywordConstants.CRON_EXPRESSION_PROPERTIES);
		String axisCornExpression=prop.getProperty(projectName.toLowerCase()+ "_trigger");
		prop.closePropertyFile();
		System.out.println(axisCornExpression);
        JobDetail axixJob = JobBuilder.newJob(PulseBotJob.class)
				  .withIdentity("Axis", "group1")
				  .build();


	    

	    	Trigger axisTrigger = TriggerBuilder
	    	.newTrigger()
	    	.withIdentity("axisAutomationTrigger", "group1")
	    	.withSchedule(CronScheduleBuilder.cronSchedule(axisCornExpression))
	    	.build();
//	    	0 31 13 ? * MON-FRI *
	    	Scheduler scheduler = new StdSchedulerFactory().getScheduler();
	    	scheduler.start();
	    	
	    	scheduler.scheduleJob(axixJob, axisTrigger);

	    }
	}

