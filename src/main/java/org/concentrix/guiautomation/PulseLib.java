package org.concentrix.guiautomation;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.github.javafaker.Faker;

import io.codearte.jfairy.Fairy;
import io.codearte.jfairy.producer.company.Company;
import io.codearte.jfairy.producer.person.Person;

public class PulseLib {
	
	EventFiringWebDriver driver;
	public PulseLib(EventFiringWebDriver driver)
	{
		this.driver=driver;
	}

	public String getPulseRandomData(String testInput1) throws Exception
	{
		String data="";
		Faker faker= new Faker();
		Fairy fairy = Fairy.create();
		Person fairyPerson = fairy.person();
		Company fairycompany = fairy.company();
		
        String output =testInput1.toLowerCase().replaceAll(" ", "");
        String testData=StringUtils.substring(output, 0,4);
        if (testData==null || testData.equals("")) {
			
        	testData="Auto";
		}
		Objects.requireNonNull(output, "Please enter test data");
		data=fairyPerson.getFirstName() + "_" + testData + "_" + fairyPerson.getLastName() + "_" + fairyPerson.getAge();

		return data;
	}

	public boolean click(String xpath)
	{
		boolean clicked=false;
		WebElement element = null;
		
		try {
			element=this.driver.findElement(By.xpath(xpath));
			if(element!=null)
			{
				element.click();
				clicked= true;
			}
		} catch (Exception e) {
			return false;
		}
		return clicked;
	}
	
}
