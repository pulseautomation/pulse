package org.concentrix.guiautomation;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;

import keyword.customobjects.TestSteps;

public class PulseDateLib {
	
	EventFiringWebDriver driver;
	Wait<EventFiringWebDriver> wait;
	public PulseDateLib(EventFiringWebDriver driver)
	{
		this.driver=driver;
		this.wait = new FluentWait<EventFiringWebDriver>(driver)
				.withTimeout(Duration.ofSeconds(50))
				.pollingEvery(Duration.ofMillis(100))
				.ignoring(WebDriverException.class)
				.ignoring(NoSuchElementException.class)
				.ignoring(ElementNotVisibleException.class)
				.ignoring(StaleElementReferenceException.class);
	}
	protected BiConsumer<TestSteps, String> testCaseLog = (testStep, logMessage) -> {
	      System.out.println(logMessage);
	      testStep.setExecutionDetails(logMessage);
	    };
	public boolean selectCalendarDate(TestSteps testStep) {
		boolean actualResult = false;
		WebElement element = null;
		try {
			String[] testDataParams = testStep.getTestData().split(",");
			String day = testDataParams[0];
			String paraMonth = testDataParams[1];
			String expMonth=getMonth(paraMonth);
			
			if(day.charAt(0)=='0'){
			
				day = String.valueOf(day.charAt(1));
			}
			int expYear = Integer.parseInt(testDataParams[2]);
			int navigationCount = getDifferenceMonthYear(expMonth, expYear);
			if (navigationCount > 0) {
				for (int i = 0; i < navigationCount; i++) {
					Thread.sleep(1000);
					List<WebElement> nextArrowList = driver.findElements(By.xpath("//a[@title='Next']"));
					for (WebElement webElement : nextArrowList) {
						for (int k = 0; k < 3; k++) {
							try {
								if (webElement.isDisplayed()) {
									webElement.click();
									break;
								}
							} catch (StaleElementReferenceException e) {
								this.wait.until(ExpectedConditions.visibilityOf(webElement));
								System.out.println("Calendar : StaleElementReferenceException");
							} 
						}
					}
							
				}
			} else if (navigationCount < 0) {
				for (int i = 0; i > navigationCount; i--) {
					Thread.sleep(1000);
					List<WebElement> nextArrowList = driver.findElements(By.xpath("//a[@title='Prev']"));
					for (WebElement webElement : nextArrowList) {
						for (int k = 0; k < 3; k++) {
							try {
						if (webElement.isDisplayed()) {
							webElement.click();
							break;
						}
							} catch (StaleElementReferenceException e) {
								this.wait.until(ExpectedConditions.visibilityOf(webElement));
								System.out.println("Calendar : StaleElementReferenceException");
							} 
						}
					}
				}
			}
			Thread.sleep(1000);
			List<WebElement> dateElements = driver.findElements(By.xpath("//div[@id=\"ui-datepicker-div\"]/table/tbody/tr/td/a[text()='" + day + "']"));
			for (WebElement webElement : dateElements) {
				System.out.println(webElement.getText());
				for (int k = 0; k < 3; k++) {
					try {

						if (webElement.isDisplayed()) {
							webElement.click();
							break;
						}
					} catch (StaleElementReferenceException e) {
				this.wait.until(ExpectedConditions.visibilityOf(webElement));
				System.out.println("Calendar : StaleElementReferenceException");
			} 
		}
			}
			actualResult = true;
			testCaseLog.accept(testStep, testStep.getTestStepDescription() +" " +testStep.getTestData());
		} catch (Exception e) {
			e.printStackTrace();
			testCaseLog.accept(testStep, e.toString());
		}

		return actualResult;
	}

	public int getDifferenceMonthYear(String month, int year) 
	{
		int diff = 0;
		Map<String, Integer> calMap = new HashMap<>();
		calMap.put("Jan", 1);
		calMap.put("Feb", 2);
		calMap.put("Mar", 3);
		calMap.put("Apr", 4);
		calMap.put("May", 5);
		calMap.put("Jun", 6);
		calMap.put("Jul", 7);
		calMap.put("Aug", 8);
		calMap.put("Sep", 9);
		calMap.put("Oct", 10);
		calMap.put("Nov", 11);
		calMap.put("Dec", 12);
		
		String displayedMonth=null;
		String displayedYear=null;
		Select select = new Select(driver.findElement(By.xpath("//select[@class='ui-datepicker-month']")));
		displayedMonth= select.getFirstSelectedOption().getText();
		int currentMonth=calMap.get(displayedMonth);
		System.out.println("Displayed month:"+currentMonth);
		int monthDiff = calMap.get(displayedMonth) - currentMonth;
		diff =  monthDiff;
		return diff;
	}

	public String getMonth(String actMonth)
	{
		Map<String, String> monthMap = new HashMap<>();
		monthMap.put("Jan", "JANUARY");
		monthMap.put("Feb", "FEBRUARY");
		monthMap.put("Mar", "MARCH");
		monthMap.put("Apr", "APRIL");
		monthMap.put("May", "MAY");
		monthMap.put("Jun", "JUNE");
		monthMap.put("Jul", "JULY");
		monthMap.put("Aug", "AUGUST");
		monthMap.put("Sep", "SEPTEMBER");
		monthMap.put("Oct", "OCTOBER");
		monthMap.put("Nov", "NOVEMBER");
		monthMap.put("Dec", "DECEMBER");
		return monthMap.get(actMonth);
	}

}
