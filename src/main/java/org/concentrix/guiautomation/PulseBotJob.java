package org.concentrix.guiautomation;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.concentrix.guiautomation.uiactions.WebApplicationActions;
import org.concentrix.guiautomation.utils.DateFormattingUtil;
import org.concentrix.guiautomation.utils.KeywordExcelUtil;
import org.concentrix.guiautomation.utils.KeywordLib;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import keyword.constants.KeywordConstants;
import keyword.customobjects.Project;
import keyword.mail.KeywordMailLib;

public class PulseBotJob implements Job {
	
	 static final String DEPLOYED_DIRECTORY="KeywordArtifacts";
	 static final Logger log = Logger.getLogger(ExecuteTests.class);
	Project project = null;
	 static final Properties GLOBAL_PROPERTIES = new Properties();
	
	static{
		String log4jConfigFile =  "resources\\"
                + File.separator + "log4j.properties";
        PropertyConfigurator.configure(log4jConfigFile);
	}

	
	@Override
	public void execute(JobExecutionContext context)
		throws JobExecutionException {
		System.out.println("Pulse Job is runing...." + LocalDateTime.now());
		String projectName = "";
		String browser = "";
		KeywordMailLib keywordMailLib = null;
		List<String> testers=null;
		String mailProperty="";
		String headless_mode="";
		
		try 
		{
			InputStream input;
			input = new FileInputStream("KeywordArtifacts/globalConfiguration.properties");
			GLOBAL_PROPERTIES.load(input);
			projectName = GLOBAL_PROPERTIES.getProperty("project");
			browser = GLOBAL_PROPERTIES.getProperty("browser");
			headless_mode=Optional.ofNullable(GLOBAL_PROPERTIES.getProperty("headless_mode")).orElse("false");
			
			Project project = new Project();
			project.setWebMode(false);
			if(headless_mode.equalsIgnoreCase("true"))
			project.setHeadlessExecution(true);
			project.setProjectName(projectName);
			project.setBrowser(browser);
			EventFiringWebDriver driver;
			WebApplicationActions application = new PulseActions(project);
			// get the email property mailexecutionreport from global config file
			// and get the testers mail ids if Yes
			 mailProperty=Optional.ofNullable(GLOBAL_PROPERTIES.getProperty("mailexecutionreport")).orElse("No");
				if(mailProperty.trim().equals("yes"))
				{
					keywordMailLib = new KeywordMailLib(KeywordConstants.MAILProperties);
					testers=keywordMailLib.gettesterMailIDs(projectName);
					project.setListOfTesters(testers);
				}

			KeywordFolders.KEYWORD_BASE_PATH = KeywordConstants.KEYWORD_BASE_PATH;
			KeywordExcelUtil excelUtil = new KeywordExcelUtil();
			
			KeywordLib keywordExecuter = new KeywordLib();
			log.info("------------------Application under Test:" + projectName + "------------------");
			DateFormattingUtil dateFormatter = new DateFormattingUtil();
			LocalDateTime startDate = LocalDateTime.now();
			project.setExecutionStartDateAndTime(startDate);
			log.info("------------------Execution started @:" + startDate + "------------------");
			log.info("--------------------------------------------------------------------------------------");
			
			String testResultPath=keywordExecuter.runner(project, application);
			application.generateReports(project);
			LocalDateTime endDate = LocalDateTime.now();
			log.info("--------------------------------------------------------------------------------------");
			log.info("------------------Execution ended @:" + endDate + "------------------");
			
			String timeTaken = dateFormatter.getTimeDifference(startDate, endDate);
			System.out.println("Time Taken : " + timeTaken);

			System.out.println("Execution Completed Successfully");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();	
//			 send the Failure Mail if any exception
			if(mailProperty.trim().equals("yes"))
			{
				String subject=keywordMailLib.getExecutionFailSubject(projectName, LocalDateTime.now());
				String body=keywordMailLib.getFailExecutionMailText(projectName, e.getMessage(), LocalDateTime.now());
				keywordMailLib.sendEmail(testers, subject, body);	
			}		
		}
	}
}
